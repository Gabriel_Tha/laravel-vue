import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// import App from './views/App'
import Example from './components/ExampleComponent.vue'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Example
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
